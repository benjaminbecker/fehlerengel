import re

def error_types(word, spec):
    result = list()
    regex = spec["regex"]
    m = re.match(regex, word)
    if m is None:
        return ["unbekannter Fehler"]
    for variant in spec["variants"]:
        if variant["match"] == m[variant["pos"]]:
            result.append(variant["error_type"])
    return result