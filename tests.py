from unittest import TestCase
from lib import error_types

class TestFindErrorVariant(TestCase):
    def test_it_detects_variants(self):
        spec = {
            "regex": r"a(g{1,2})re(s{1,2})i(v|f)",
            "variants": [
                {"pos": 1, "match": "g", "error_type": "Doppel-g-Vergesser"},
                {"pos": 2, "match": "s", "error_type": "Doppel-s-Vergesser"},
                {"pos": 3, "match": "f", "error_type": "Verwechslung: f statt v"},
            ]
        }
        self.assertEqual(
            error_types("aggressiv", spec),
            []
        )
        self.assertEqual(
            error_types("bla", spec),
            ["unbekannter Fehler"]
        )
        self.assertEqual(
            error_types("agressiv", spec),
            ["Doppel-g-Vergesser"]
        )
        self.assertEqual(
            error_types("agresif", spec),
            ["Doppel-g-Vergesser", "Doppel-s-Vergesser", "Verwechslung: f statt v"]
        )